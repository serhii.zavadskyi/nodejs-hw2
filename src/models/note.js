const mongoose = require('mongoose');

const Note = mongoose.model('Note', {
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
    trim: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User',
  },
});

module.exports = Note;
