const express = require('express');
const User = require('../models/user');

const router = new express.Router();

router.post('/api/auth/register', async (req, res) => {
  const user = new User(req.body);

  try {
    await user.save();
    const token = await user.generateAuthToken();
    res.status(200).json({message: 'Success', token});
  } catch (err) {
    res.status(400).send(err.message);
  }
});

router.post('/api/auth/login', async (req, res) => {
  try {
    const user = await User.findByCredentials(
        req.body.username,
        req.body.password,
    );
    const token = await user.generateAuthToken();

    res.status(200).json({message: 'Success', jwt_token: token});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
});

module.exports = router;
