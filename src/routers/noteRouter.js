const express = require('express');
const Note = require('../models/note');
const auth = require('../middleware/auth');
const router = new express.Router();

router.post('/api/notes', auth, async (req, res) => {
  const note = new Note({...req.body, userId: req.user._id});

  try {
    await note.save();
    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(400).json({message: 'Something went wrong'});
  }
});

router.get('/api/notes', auth, async (req, res) => {
  try {
    await req.user.populate({
      path: 'notes',
      options: {
        skip: parseInt(req.query.offset),
        limit: parseInt(req.query.limit),
      },
    }).execPopulate();
    res.json({notes: req.user.notes});
  } catch (error) {
    res.status(500).json({message: 'Something went wrong'});
  }
});

router.get('/api/notes/:id', auth, async (req, res) => {
  const _id = req.params.id;

  try {
    const note = await Note.findOne({_id, userId: req.user._id});

    if (!note) {
      return res.status(400).json({message: 'Note not found'});
    }

    res.json({note: note});
  } catch (error) {
    res.status(500).json({message: 'Something went wrong'});
  }
});

router.put('/api/notes/:id', auth, async (req, res) => {
  const updates = Object.keys(req.body);
  const allowedUpdats = ['text'];
  const isValidOperation = updates.every((update) =>
    allowedUpdats.includes(update),
  );

  if (!isValidOperation) {
    return res.status(400).json({message: 'Invalid updates'});
  }

  try {
    const note = await Note.findOne({
      _id: req.params.id,
      userId: req.user._id,
    });

    if (!note) {
      return res.status(400).json({message: 'Note not found'});
    }

    updates.forEach((update) => (note[update] = req.body[update]));

    await note.save();

    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(400).json({message: 'Something went wrong'});
  }
});

router.patch('/api/notes/:id', auth, async (req, res) => {
  const updates = Object.keys(req.body);
  const allowedUpdats = ['completed'];
  const isValidOperation = updates.every((update) =>
    allowedUpdats.includes(update),
  );

  if (!isValidOperation) {
    return res.status(400).json({message: 'Invalid updates'});
  }

  try {
    const note = await Note.findOne({
      _id: req.params.id,
      userId: req.user._id,
    });

    if (!note) {
      return res.status(400).json({message: 'Note not found'});
    }
    note.completed = !note.completed;
    await note.save();

    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(400).json({message: 'Something went wrong'});
  }
});

router.delete('/api/notes/:id', auth, async (req, res) => {
  try {
    const note = await Note.findOneAndDelete({
      _id: req.params.id,
      userId: req.user._id,
    });

    if (!note) {
      return res.status(400).json({message: 'Note not found'});
    }

    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(500).json({message: 'Something went wrong'});
  }
});

module.exports = router;
