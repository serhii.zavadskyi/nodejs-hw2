const express = require('express');
const auth = require('../middleware/auth');

const bcrypt = require('bcrypt');

const router = new express.Router();

router.get('/api/users/me', auth, async (req, res) => {
  res.status(200).json({user: req.user});
});

router.post('/api/users/logout', auth, async (req, res) => {
  try {
    req.user.tokens = req.user.tokens.filter(
        (token) => token.token !== req.token,
    );
    await req.user.save();

    res.json({message: 'User logout'});
  } catch (error) {
    res.status(500).json({message: 'Something went wrong'});
  }
});

router.patch('/api/users/me', auth, async (req, res) => {
  const {oldPassword, newPassword} = req.body;

  try {
    if (!oldPassword) {
      return res.status(400).json({
        message: 'Parameter oldPassword is not defined',
      });
    }

    if (!newPassword) {
      return res.status(400).json({
        message: 'Parameter newPassword is not defined',
      });
    }

    if (!(await bcrypt.compare(oldPassword, req.user.password))) {
      return res.status(400).json({message: `Wrong old password!`});
    }

    if (newPassword !== oldPassword) {
      req.user.password = newPassword;
      await req.user.save();
      res.status(200).json({message: 'Success'});
    }
  } catch (error) {
    res.status(400).json({message: 'Something went wrong'});
  }
});

router.delete('/api/users/me', auth, async (req, res) => {
  try {
    await req.user.remove();
    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(500).json({message: 'Something went wrong'});
  }
});

module.exports = router;
