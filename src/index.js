const express = require('express');
require('./db/mongoose');
const morgan = require('morgan');

const userRouter = require('./routers/userRouter');
const noteRouter = require('./routers/noteRouter');
const authRouter = require('./routers/authRouter');


const app = express();
const PORT = 8080;

app.use(morgan('tiny'));
app.use(express.json());

app.use(userRouter);
app.use(noteRouter);
app.use(authRouter);

app.listen(PORT, () => {
  console.log(`App listening at http://localhost:${PORT}`);
});
